# README #

### What is this repository for? ###

Ebenezer Plaza Energy Model for EGC certification using eQuest v 3.65 

### Building Description ###

Phase I (Site A) of the project involves ground up construction of two affordable residential buildings located on tax block 3682 in Brooklyn, NY. 
Building A1 is proposed as a 9-story tower with 113,431 GSF of residential apartments and 8,400 GSF retail program. Building A2 is proposed as a 11-story tower with 166,163 GSF of residential apartments. 
Both towers will share a common podium that will be home to a 34,000 GSF church. The total combined area covered in this Proposal is approximately 323,922 GSF.


